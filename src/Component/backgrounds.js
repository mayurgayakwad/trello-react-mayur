import React, { useState } from "react";
import DoneIcon from "@mui/icons-material/Done";
const backgroundArray = [
  {
    key: "blue",
    backgroundColor: "rgb(0, 121, 191)",
    backgrondImage:
      "https://images.unsplash.com/photo-1557683316-973673baf926?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1729&q=80",
  },
  {
    key: "orange",
    backgroundColor: "rgb(210, 144, 52)",
    backgrondImage:
      "https://images.unsplash.com/photo-1557682250-33bd709cbe85?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1729&q=80",
  },
  {
    key: "green",
    backgroundColor: "rgb(81, 152, 57)",
    backgrondImage:
      "https://images.unsplash.com/photo-1557683311-eac922347aa1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1729&q=80",
  },
  {
    key: "red",
    backgroundColor: "rgb(176, 70, 50)",
    backgrondImage:
      "https://images.unsplash.com/photo-1557682260-96773eb01377?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1729&q=80",
  },
  {
    key: "purple",
    backgroundColor: "rgb(137, 96, 158)",
    backgrondImage:
      "https://images.unsplash.com/photo-1672009190560-12e7bade8d09?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1770&q=80",
  },
  {
    key: "pink",
    backgroundColor: "rgb(205, 90, 145)",
    backgrondImage:
      "https://images.unsplash.com/photo-1579548122080-c35fd6820ecb?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1770&q=80",
  },
  {
    key: "sky",
    backgroundColor: "rgb(0, 174, 204",
    backgrondImage:
      "https://images.unsplash.com/photo-1579548122080-c35fd6820ecb?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1770&q=80",
  },
  {
    key: "gray",
    backgroundColor: "rgb(131, 140, 145)",
    backgrondImage:
      "https://images.unsplash.com/photo-1579548122080-c35fd6820ecb?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1770&q=80",
  },
];

function BackgroundComponent(props) {
  const [selectedBackgrounds, setSelectedBackgrounds] = useState({key : 'blue'});
  //   const handleIconClick = (background) => {
  //     setBackgroundSelect(background);
  //     props.backgroundImageSelect(background.key);
  //   };
  const handleClick = (background) => {
    if (selectedBackgrounds === background.key) {
      // If the clicked background is already selected, deselect it
      setSelectedBackgrounds(null);
      props.backgroundColorSelect(backgroundArray[0]);
    } else {
      // If a different background is clicked, select it
      setSelectedBackgrounds(background.key);
      props.backgroundColorSelect(background);
    }
  };
  return (
    <div
      className="flex gap-2 flex-wrap w-[285px]"
      style={{ margin: "0 1rem 0.3rem" }}
    >
      {backgroundArray.map((background) => (
        <div
          title={background.key}
          key={background.key}
          className={`border w-[64px] h-[40px] rounded-[5px] shadow-md cursor-pointer`}
          style={{ backgroundColor: background.backgroundColor }}
          onClick={() => handleClick(background)}
        >
          <span>
            {selectedBackgrounds === background.key && (
              <DoneIcon
                style={{
                  fontSize: "small",
                  position: "relative",
                  top: "5px",
                  left: "25px",
                  color: "white",
                }}
              />
            )}
          </span>
        </div>
      ))}
    </div>
  );
}

export default BackgroundComponent;
