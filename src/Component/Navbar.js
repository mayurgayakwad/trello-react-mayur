import { AppBar, Toolbar, CardMedia, Search, Divider } from "@mui/material";
import React, { Component } from "react";
import AppsIcon from "@mui/icons-material/Apps";
import SearchIcon from "@mui/icons-material/Search";
import SvgIcon from "@mui/material/SvgIcon";
import { Link } from "react-router-dom";
import "./File.css";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
    };
  }

  render() {
    return (
      <>
        <AppBar
          style={{ paddingLeft: "12px !important" }}
          className="h-12 flex justify-center"
          position="static"
        >
          <Toolbar>
            <AppsIcon />
            <Link to={"/"}>
              {" "}
              <CardMedia
                component="img"
                flexgrow={1}
                alt="logo not found"
                height=""
                sx={{ width: 100, marginLeft: 2 }}
                image="https://a.trellocdn.com/prgb/dist/images/header-logo-spirit.d947df93bc055849898e.gif"
              />
            </Link>
            {this.props.boardName && (
              <div
                style={{
                  marginLeft: "98px",
                  paddingLeft: "30px",
                  fontSize: "18px",
                  fontWeight: "bold",
                  borderLeft: "1px solid white",
                }}
              >
                {this.props.boardName}
              </div>
            )}
          </Toolbar>
        </AppBar>
        <Divider
          flexItem
          style={{ backgroundColor: "rgba(255, 255, 255, 0.40)" }}
        />
      </>
    );
  }
}

export default Navbar;
