import React, { Component } from "react";
import CloseIcon from "@mui/icons-material/Close";
import styled from "@emotion/styled";
import { Button } from "@mui/material";
import Menu from "@mui/material/Menu";
import AssignmentTurnedInIcon from "@mui/icons-material/AssignmentTurnedIn";
import CreditScoreIcon from "@mui/icons-material/CreditScore";
import { Dialog, DialogTitle } from "@mui/material";
import { Stack, DialogContent, DialogActions } from "@mui/material";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

import { Box } from "@mui/system";
import { OutlinedInput } from "@mui/material";
import MenuItem from "@mui/material/MenuItem";

import { TextField, Typography } from "@mui/material";
import Loader from "./Loader";

const key = "ba0effa96c13b536f613cc58d7c7dad9";
const token =
  "ATTAb83a3218b7c8df820b9721f3d9a6b3227907f128eb8e5604a6704c7c4aa2eefcC8E6FC13";

const ListBox = styled(Box)({
  background: "white",
  padding: "5px",
  color: "black",
  marginBottom: "10px",
  borderRadius: "2px",
  boxShadow: "1px 1px 4px gray",
});
class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CardsList: [],
      checkListData: [],
      fetchingChecklist: false,
      newCardName: "",
      addacard: false,
      add: false,
      open: false,
      openChecklist: false,
      openChecklistAdd: false,
      openChecklistItem: false,
      error: false,
      cardName: "",
      cardsID: "",
      createChecklistName: "",
      ChecklistName: "",
      ChecklistID: "",
      createChecklistItemName: "",
    };
  }

  handelOpen = () => {
    this.setState({
      addacard: true,
    });
  };

  openChecklistAdd = () => {
    this.setState({
      openChecklistAdd: true,
      openChecklistItem: false,
    });
  };
  openChecklistItem = () => {
    this.setState({
      openChecklistItem: true,
      openChecklistAdd: false,
    });
  };
  CloseChecklistAdd = () => {
    this.setState({
      openChecklistAdd: false,
    });
  };

  closeChecklistItem = () => {
    this.setState({
      openChecklistItem: false,
      ChecklistName: "",
    });
  };
  handleClose = () => {
    this.setState({
      addacard: false,
      add: false,
      open: false,
      openChecklist: false,
      cardsID: "",
      cardName: "",
    });
  };

  newCardName = (event) => {
    this.setState({
      newCardName: event.target.value,
    });
  };
  createChecklistName = (event) => {
    this.setState({
      createChecklistName: event.target.value,
    });
  };
  createChecklistItemName = (event) => {
    this.setState({
      createChecklistItemName: event.target.value,
    });
  };

  gettingCards = () => {
    fetch(
      `https://api.trello.com/1/lists/${this.props.listid}/cards?key=${key}&token=${token}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        this.setState({
          CardsList: JSON.parse(text),
        });
      })
      .catch((err) => console.error(err));
  };
  createNewCard = () => {
    fetch(
      `https://api.trello.com/1/cards?idList=${this.props.listid}&name=${this.state.newCardName}&key=${key}&token=${token}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        const prevCards = [...this.state.CardsList];
        prevCards.push(JSON.parse(text));
        this.setState({
          CardsList: prevCards,
          newCardName: "",
        });
      })
      .catch((err) => console.error(err));
    this.gettingCards();
  };
  HandleCreatecard = () => {
    if (this.state.newCardName) {
      this.createNewCard();
      this.setState({
        addacard: false,
      });
    }
  };

  createChecklist = () => {
    fetch(
      `https://api.trello.com/1/checklists?name=${this.state.createChecklistName}&idCard=${this.state.cardsID}&key=${key}&token=${token}`,
      {
        method: "POST",
      }
    )
      .then((response) => response.text())
      .then((text) => {
        let newCheckLists = [];
        if (this.state.checkListData.length > 0) {
          newCheckLists = [...this.state.checkListData];
        }
        newCheckLists.push(text);
        this.setState({
          openChecklistAdd: false,
          checkItemData: newCheckLists,
          createChecklistName: "",
        });
      })
      .catch((err) => console.error(err));
  };
  getsChecklist = (cardsID) => {
    this.setState({ fetchingChecklist: true });
    console.log(cardsID);
    fetch(
      `https://api.trello.com/1/cards/${cardsID}/checklists?key=${key}&token=${token}`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        this.setState({
          checkListData: JSON.parse(text),
          fetchingChecklist: false,
        });
      })
      .catch((err) => console.error(err));
  };
  createsNewCheckItems = () => {
    // console.log(checklistID);
    fetch(
      `https://api.trello.com/1/checklists/${this.state.ChecklistID}/checkItems?name=${this.state.createChecklistItemName}&key=${key}&token=${token}`,
      {
        method: "POST",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        let newCheckLists = [];
        if (this.state.checkListData.length > 0) {
          newCheckLists = [...this.state.checkListData];
        }
        newCheckLists.push(text);
        this.setState({
          openChecklistItem: false,
          checkItemData: newCheckLists,
        });
      })
      .catch((err) => console.error(err));
  };
  openChecklist = () => {
    this.setState({
      openChecklist: true,
    });
  };
  componentDidMount() {
    this.gettingCards();
  }

  deleteCard = (id) => {
    fetch(`https://api.trello.com/1/cards/${id}?key=${key}&token=${token}`, {
      method: "DELETE",
    })
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        const newCards = this.state.CardsList.filter((card) => {
          return card.id !== id;
        });
        this.setState({ CardsList: newCards });
      })
      .catch((err) => console.error(err));
  };

  deleteChecklist = (id) => {
    fetch(
      `https://api.trello.com/1/checklists/${id}?key=${key}&token=${token}`,
      {
        method: "DELETE",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => console.log(text))
      .catch((err) => console.error(err));
  };
  deleteChecklistItem = (ChecklistID, checklistItemId) => {
    console.log(ChecklistID);
    console.log(checklistItemId);
    fetch(
      `https://api.trello.com/1/checklists/${ChecklistID}/checkItems/${checklistItemId}?key=${key}&token=${token}`,
      {
        method: "DELETE",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => console.log(text))
      .catch((err) => console.error(err));
  };

  render() {
    // console.log(this.state.checkItemData);
    return (
      <>
        {this.state.CardsList ? (
          <>
            {this.state.CardsList.map((cards, index) => {
              return (
                <>
                  <ListBox style={{ borderRadius: "6px" }}>
                    <div
                      style={{
                        display: "flex ",
                        justifyContent: "space-between",
                        cursor: "pointer",
                        borderRadius: "8px",
                      }}
                      className=""
                      key={cards.id}
                    >
                      <Typography
                        id={cards.id}
                        onClick={(e) => {
                          this.setState({
                            cardName: cards.name,
                            cardsID: cards.id,
                          });
                          this.openChecklist(e);
                          this.getsChecklist(cards.id);
                        }}
                        width={225}
                        sx={{ paddingLeft: "5px" }}
                      >
                        {cards.name}
                      </Typography>
                      <DeleteForeverIcon
                        style={{ cursor: "pointer" }}
                        className="hover:text-red-700"
                        onClick={() => {
                          this.deleteCard(cards.id);
                        }}
                      ></DeleteForeverIcon>
                    </div>
                  </ListBox>
                  <Stack>
                    <Dialog
                      open={this.state.openChecklist}
                      onClose={this.handleClose}
                    >
                      <DialogTitle style={{ width: "552px", height: "" }}>
                        <CreditScoreIcon style={{ marginRight: "20px" }} />
                        {this.state.cardName}
                      </DialogTitle>
                      <hr />
                      {this.state.fetchingChecklist &&
                        !this.state.checkListData.length && <Loader />}
                      {!!this.state.checkListData.length &&
                        this.state.checkListData.map((checklist) => {
                          return (
                            <>
                              <ListBox style={{ margin: "10px" }}>
                                <div
                                  style={{
                                    display: "flex ",
                                    justifyContent: "space-between",
                                  }}
                                  className=""
                                  key={cards.id}
                                >
                                  <Typography
                                    id={cards.id}
                                    style={{
                                      width: "225px",
                                      fontSize: "16px",
                                      fontWeight: "bold",
                                    }}
                                  >
                                    {checklist.name}
                                  </Typography>
                                  <DeleteForeverIcon
                                    style={{ cursor: "pointer" }}
                                    className="hover:text-red-700"
                                    onClick={() => {
                                      this.deleteChecklist(checklist.id);
                                      // this.getsCheckItems(checklist.id);
                                    }}
                                  ></DeleteForeverIcon>
                                </div>
                                <hr />
                                <div>
                                  {checklist.checkItems.map((checkItem) => {
                                    return (
                                      <div className="flex items-center ">
                                        <Typography
                                          style={{
                                            width: "500px",
                                          }}
                                        >
                                          {checkItem.name}
                                        </Typography>

                                        <DeleteForeverIcon
                                          className=" cursor-pointer hover:text-red-700"
                                          onClick={() => {
                                            this.deleteChecklistItem(
                                              checklist.id,
                                              checkItem.id
                                            );
                                          }}
                                        />
                                      </div>
                                    );
                                  })}
                                </div>
                              </ListBox>
                              <Button
                                variant="outlined"
                                style={{
                                  width: "140px",
                                  marginLeft: "10px",
                                  display: "flex",
                                  float: "right",
                                }}
                                // onClick={this.openChecklistItem}
                                onClick={(e) => {
                                  this.setState({
                                    ChecklistName: checklist.name,
                                    ChecklistID: checklist.id,
                                  });
                                  // this.openChecklist(e);
                                  this.openChecklistItem(e);

                                  // this.createsNewCheckItems(checklist.id);
                                  // this.getsChecklist(cards.id);
                                }}
                              >
                                Add an item
                              </Button>
                            </>
                          );
                        })}

                      <DialogActions>
                        <Button
                          style={{
                            width: "125px",
                          }}
                          variant="contained"
                          onClick={this.openChecklistAdd}
                        >
                          <AssignmentTurnedInIcon />
                          checklist
                        </Button>
                        <Button onClick={this.handleClose}>Cancel</Button>
                      </DialogActions>
                    </Dialog>
                  </Stack>
                  <div className="px-10">
                    <Menu
                      id="basic-menu"
                      open={this.state.openChecklistAdd}
                      onClose={this.state.handleClose}
                      MenuListProps={{
                        "aria-labelledby": "basic-button",
                      }}
                      style={{
                        alignItems: "center",
                        justifyContent: "center",
                        position: "absolute",
                        left: "50%",
                        transform: "translate(15%,-50%)",
                      }}
                    >
                      <div className="flex w-[325px] justify-between">
                        <Typography paddingLeft={15}>Add checklist</Typography>{" "}
                        <MenuItem>
                          <CloseIcon
                            onClick={this.CloseChecklistAdd}
                            className="h-2 float-right pb-2"
                          />
                        </MenuItem>
                      </div>
                      <hr />
                      <Typography
                        style={{ fontWeight: "bold", paddingLeft: "20px" }}
                      >
                        Tilte
                      </Typography>
                      <OutlinedInput
                        onChange={this.createChecklistName}
                        className="w-[90%] ml-4 h-10 text-xl"
                      />
                      <Button
                        style={{
                          fontWeight: "bold",
                          marginLeft: "20px",
                          marginTop: "6px",
                        }}
                        variant="contained"
                        size="small"
                        onClick={this.createChecklist}
                      >
                        Add
                      </Button>
                    </Menu>
                  </div>
                  <div className="px-10">
                    <Menu
                      id="basic-menu"
                      open={this.state.openChecklistItem}
                      onClose={this.state.handleClose}
                      MenuListProps={{
                        "aria-labelledby": "basic-button",
                      }}
                      style={{
                        alignItems: "center",
                        justifyContent: "center",
                        position: "absolute",
                        left: "50%",
                        transform: "translate(15%,-50%)",
                      }}
                    >
                      <div className="flex w-[325px] justify-between">
                        <Typography paddingLeft={15}>
                          {this.state.ChecklistName}
                        </Typography>
                        <MenuItem>
                          <CloseIcon
                            onClick={this.closeChecklistItem}
                            className="h-2 float-right pb-2"
                          />
                        </MenuItem>
                      </div>
                      <hr />
                      <Typography
                        style={{ fontWeight: "bold", paddingLeft: "20px" }}
                      >
                        Add an Item
                      </Typography>
                      <OutlinedInput
                        onChange={this.createChecklistItemName}
                        className="w-[90%] ml-4 h-10 text-xl"
                      />
                      <Button
                        style={{
                          fontWeight: "bold",
                          marginLeft: "20px",
                          marginTop: "6px",
                        }}
                        variant="contained"
                        size="small"
                        onClick={this.createsNewCheckItems}
                      >
                        Add
                      </Button>
                    </Menu>
                  </div>
                </>
              );
            })}
          </>
        ) : (
          "Loading....."
        )}
        {this.state.addacard ? (
          <>
            <TextField
              id="outlined-basic"
              size="small"
              fullWidth
              label="Enter a title for this card…"
              type={"textarea"}
              variant="outlined"
              onChange={this.newCardName}
              value={this.state.newCardName}
            />
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                marginTop: "8px",
              }}
            >
              <Button
                variant="contained"
                size="small"
                onClick={this.HandleCreatecard}
              >
                Add Card
              </Button>
              <CloseIcon
                onClick={this.handleClose}
                className="m-2 cursor-pointer"
              ></CloseIcon>
            </div>
          </>
        ) : (
          <Button
            variant="text"
            size="small"
            onClick={this.handelOpen}
            className="text-secondary"
          >
            + Add a Card
          </Button>
        )}
      </>
    );
  }
}

export default Cards;
