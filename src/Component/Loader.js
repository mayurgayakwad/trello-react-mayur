import React from "react";
import './LoaderStyle.css'

function Loader(props) {
  return (
    <>
      <div class="loader" style={props.style}>
        <div class="justify-content-center jimu-primary-loading"></div>
      </div>
    </>
  );
}

export default Loader;
