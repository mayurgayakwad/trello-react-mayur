import { Typography, CardMedia, OutlinedInput } from "@mui/material";
import { Box } from "@mui/system";
import React, { Component } from "react";
import Menu from "@mui/material/Menu";
import CircularProgress from "@mui/material/CircularProgress";
import MenuItem from "@mui/material/MenuItem";
import CloseIcon from "@mui/icons-material/Close";
import "./File.css";
import { Link } from "react-router-dom";
import Loader from "./Loader";

import BackgroundComponent from "./backgrounds";

const key = "ba0effa96c13b536f613cc58d7c7dad9";
const token =
  "ATTAb83a3218b7c8df820b9721f3d9a6b3227907f128eb8e5604a6704c7c4aa2eefcC8E6FC13";

class YourWorkSpace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      input: "",
      listOfBoards: [],
      isDataFetching: false,
      background: {
        key: "blue",
        backgroundColor: "rgb(0, 121, 191)",
      },
    };
  }

  openCloseHandler = (event) => {
    console.log("lkjhgfd", this.state.open);
    this.setState((prevState) => {
      return { open: !prevState.open };
    });
  };
  handleInput = (event) => {
    this.setState({
      input: event.target.value,
    });
  };

  makeNewBords = () => {
    fetch(
      `https://api.trello.com/1/boards/?name=${this.state.input}&prefs_background=${this.state.background.key}&key=${key}&token=${token}`,
      {
        method: "POST",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        console.log({ text });
        this.setState({
          open: false,
          input: "",
        });
        const id = JSON.parse(text).id;
        console.log({ id });
        location.replace(`/${id}`);
      })
      .catch((err) => alert("Problem For Creating Board!"));
  };
  getBoardsId = () => {
    this.setState({ isDataFetching: true });
    fetch(
      `https://api.trello.com/1/members/me/boards?fields=name,url,prefs&key=${key}&token=${token}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        this.setState({
          listOfBoards: JSON.parse(text),
          isDataFetching: false,
        });
      })
      .catch((err) => {
        console.error(err),
          this.setState({
            isDataFetching: false,
          });
      });
  };
  backgroundColorSelect = (background) => {
    console.log({ background });
    this.setState({
      background: {
        key: background.key,
        backgroundColor: background.backgroundColor,
      },
    });
  };

  componentDidMount() {
    this.getBoardsId();
  }
  render() {
    return (
      <>
        <Box>
          <Typography fontWeight="bold" color={"#5e6c84"} marginY="25px">
            YOUR WORKSPACES
          </Typography>
          <div className="flex gap-2 items-center mb-10">
            <Typography
              border={1}
              textAlign="center"
              color={"white"}
              fontSize={"20px"}
              bgcolor={"#00875a"}
              fontWeight="bold"
              width={40}
            >
              T
            </Typography>
            <Typography fontWeight={700}>Trello Workspace</Typography>
          </div>
          <div className="flex gap-2 flex-wrap">
            {this.state.isDataFetching && (
              <Loader style={{ right: "24rem", bottom: "24.5rem" }} />
            )}
            {!!this.state.listOfBoards.length &&
              this.state.listOfBoards.map((boards) => (
                <Link to={`${boards.id}`}>
                  <div
                    className="border w-[200px] h-[94px] rounded-[5px] text-white text-[16px] font-bold shadow-md"
                    style={{ backgroundColor: boards.prefs.backgroundColor }}
                  >
                    <div title={boards.name} className="pl-2 pt-1 w-full">
                      {boards.name.length > 42
                        ? `${boards.name.substring(0, 43)}...`
                        : boards.name}
                    </div>
                  </div>
                </Link>
              ))}
            <button
              variant="outlined"
              className="border-none w-[200px] rounded-[5px] h-24 bg-[#091e420] shadow-md"
              aria-controls={this.state.open ? "basic-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={this.state.open ? "true" : undefined}
              onClick={this.openCloseHandler}
              disabled={this.state.listOfBoards.length === 10}
            >
              {!this.state.isDataFetching && "Create new board"}
              {!this.state.isDataFetching && (
                <div>{10 - this.state.listOfBoards.length} remaining</div>
              )}
            </button>
          </div>
          <div className="px-10">
            <Menu
              // id="basic-menu"
              open={this.state.open}
              onClose={this.state.handleClose}
              MenuListProps={{
                "aria-labelledby": "basic-button",
              }}
              onClickAway={this.openCloseHandler}
              style={{
                alignItems: "center",
                justifyContent: "center",
                position: "absolute",

                left: "50%",
                transform: "translate(-10%, 0)",
              }}
            >
              <div className="flex w-[325px] justify-between">
                <Typography paddingLeft={14} className="create-board"></Typography>{" "}
                <MenuItem>
                  {" "}
                  <CloseIcon
                    onClick={this.openCloseHandler}
                    className="h-2 float-right pb-2"
                  />
                </MenuItem>
              </div>
              <hr />
              <div class="image-container">
                <div
                  alt="Bottom Image"
                  class="bottom-image"
                  style={{
                    width: "200px",
                    height: "120px",
                    backgroundColor: `${this.state.background.backgroundColor}`,
                  }}
                />
                <img
                  src={
                    "https://a.trellocdn.com/prgb/dist/images/board-preview-skeleton.14cda5dc635d1f13bc48.svg"
                  }
                  alt="Top Image"
                  class="top-image"
                />
              </div>
              <Typography
                paddingX={2}
                style={{ fontSize: "14px", fontWeight: "600", color: "gray" }}
              >
                Background
              </Typography>
              {
                <BackgroundComponent
                  backgroundColorSelect={(background) =>
                    this.backgroundColorSelect(background)
                  }
                />
              }
              <Typography
                paddingX={2}
                style={{ fontSize: "14px", fontWeight: "600", color: "gray" }}
              >
                Board title
                <span className="text-[#eb5a46] text-base ml-1">*</span>
              </Typography>

              <OutlinedInput
                onChange={this.handleInput}
                className="w-[90%] ml-4 h-10 text-xl"
                type="text"
              />
              {this.state.input.length > 0 ? (
                ""
              ) : (
                <Typography
                  className="pl-4 pt-2"
                  style={{ fontSize: "14px", fontWeight: "500" }}
                >
                  👋 Board title is required
                </Typography>
              )}

              {this.state.input.length > 0 ? (
                <button
                  onClick={this.makeNewBords}
                  className="w-[90%] border-none ml-4 my-4 bg-[#0079bf] rounded-sm text-white h-10 text-base shadow-md"
                >
                  Create
                </button>
              ) : (
                <button className="w-[90%] shadow-md text-gray-400 border-none ml-4 my-2 cursor-not-allowed rounded-sm bg-slate-100 h-10 text-base">
                  Create
                </button>
              )}
            </Menu>
          </div>
        </Box>
      </>
    );
  }
}

export default YourWorkSpace;
