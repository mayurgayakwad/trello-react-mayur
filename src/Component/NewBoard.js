import React, { Component } from "react";
import styled from "@emotion/styled";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

import {
  Button,
  Dialog,
  DialogTitle,
  Typography,
  Divider,
} from "@mui/material";
import { Stack, DialogContent, TextField, DialogActions } from "@mui/material";

import Navbar from "./Navbar";
import Cards from "./Cards";
import Loader from "./Loader";
import { Link } from "react-router-dom";
import boardsIcon from "../Component/Images/boards.svg";
import membersIcon from "../Component/Images/member.svg";
import settingIcon from "../Component/Images/setting.svg";

const key = "ba0effa96c13b536f613cc58d7c7dad9";
const token =
  "ATTAb83a3218b7c8df820b9721f3d9a6b3227907f128eb8e5604a6704c7c4aa2eefcC8E6FC13";

const gradientBackground = {
  background: "linear-gradient(#79ce6f, #0ba29d)",
};
const topContainer = [
  {
    filedName: "Boards",
    iconSrc: boardsIcon,
  },
  {
    filedName: "Members",
    iconSrc: membersIcon,
  },
  {
    filedName: "Workspace settings",
    iconSrc: settingIcon,
  },
];
class NewBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allList: [],
      open: false,
      listName: "",
      backgroundColor: "",
      boardName: "",
      fetchingDataLoader: false,
      listOfBoards: [],
    };
  }

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  };
  handleClose = () => {
    this.setState({
      open: false,
    });
  };
  newListName = (event) => {
    this.setState({
      listName: event.target.value,
    });
  };

  getBoardsId = () => {
    this.setState({ isDataFetching: true });
    const { id } = this.props.match.params;
    fetch(
      `https://api.trello.com/1/members/me/boards?fields=name,url,prefs&key=${key}&token=${token}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        this.setState({
          listOfBoards: JSON.parse(text).filter((border) => border.id != id),
          isDataFetching: false,
        });
      })
      .catch((err) => {
        console.error(err),
          this.setState({
            isDataFetching: false,
          });
      });
  };
  createNewList = () => {
    const { id } = this.props.match.params;
    fetch(
      `https://api.trello.com/1/lists?name=${this.state.listName}&idBoard=${id}&key=${key}&token=${token}`,
      {
        method: "POST",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        const prevList = [...this.state.allList];
        prevList.push(JSON.parse(text));
        this.setState({
          allList: prevList,
          open: false,
        });

        // this.setState({ data:  });
      })
      .catch((err) => console.error(err));
  };

  deleteList = (id) => {
    fetch(
      `https://api.trello.com/1/lists/${id}/closed?value=true&key=${key}&token=${token}`,
      {
        method: "PUT",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        const newLists = this.state.allList.filter((dellist) => {
          return dellist.id !== id;
        });
        this.setState({ allList: newLists });
      })
      .catch((err) => console.error(err));
  };
  createButton = () => {
    this.handleClose();
    this.createNewList();
  };

  componentDidMount() {
    this.getBoardsId();
    const { id } = this.props.match.params;
    this.setState({
      fetchingDataLoader: true,
    });
    fetch(`https://api.trello.com/1/boards/${id}?key=${key}&token=${token}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    })
      .then((response) => {
        console.log(
          `Response: ${response} ${response.status} ${response.statusText}`
        );
        return response.text();
      })
      .then((text) => {
        const data = JSON.parse(text);
        if (data) {
          this.setState({
            backgroundColor: data.prefs.backgroundColor,
            boardName: data.name,
            fetchingDataLoader: false,
          });
        }
      })
      .catch((err) => console.error(err));

    fetch(
      `https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        console.log(
          `Response: ${response} ${response.status} ${response.statusText}`
        );
        return response.text();
      })
      .then((text) => {
        console.log({ text }, JSON.parse(text));
        this.setState({
          allList: JSON.parse(text),
          fetchingDataLoader: false,
        });
      })
      .catch((err) => console.error(err));
  }
  render() {
    console.log(this.props.match.params);
    return (
      <>
        <Navbar boardName={this.state.boardName} />
        <div
          className="h-[100vh] flex bg-cover bg-no-repeat"
          style={{ backgroundColor: `${this.state.backgroundColor}` }}
        >
          <div className="flex flex-col gap-2 w-[250px]">
            <div className="flex gap-2 items-center w-[250px] p-2">
              <Typography
                textAlign="center"
                color={"white"}
                fontSize={"20px"}
                bgcolor={"#00875a"}
                fontWeight="bold"
                width={32}
                height={32}
                borderRadius="4px"
                style={gradientBackground}
              >
                T
              </Typography>
              <Typography fontWeight={700} color={"white"}>
                Trello Workspace
              </Typography>
            </div>
            <Divider style={{ backgroundColor: "rgba(255, 255, 255, 0.40)" }} />
            {topContainer.map((ele) => (
              <div className="flex pl-[6px] place-items-center hover:bg-[#FFFFFF3D]  hover:rounded-[5px]">
                <img key={ele.filedName} src={ele.iconSrc} alt="Icon" />
                <Link to={`/`}>
                  <div
                    style={{
                      height: "32px",
                      width: "210px",
                      // background: "#FFFFFF3D",
                      textTransform: "none",
                      // borderRadius: "6px",
                      color: "#FFFFFF",
                      padding: "6px 8px",
                      fontSize: "14px",
                    }}
                  >
                    <div title={ele.filedName} className="w-full">
                      {ele.filedName}
                    </div>
                  </div>
                </Link>
              </div>
            ))}
            <Typography
              style={{
                paddingLeft: 10,
                fontSize: "14px",
                fontWeight: "600",
                color: "#ffffff",
              }}
            >
              Your boards
            </Typography>
            {!!this.state.listOfBoards.length &&
              this.state.listOfBoards.map((boards) => (
                <>
                  <div className="flex place-items-center hover:bg-[#FFFFFF3D]  hover:rounded-[5px] hover:cursor-pointer">
                    <div
                      style={{
                        backgroundColor: boards.prefs.backgroundColor,
                        minHeight: 25,
                        minWidth: 30,
                        margin: 5,
                      }}
                    ></div>
                    {/* <Link to={`${boards.id}`}> */}
                    <div
                      style={{
                        height: "32px",
                        width: "210px",
                        // background: "#FFFFFF3D",
                        textTransform: "none",
                        // borderRadius: "6px",
                        color: "#FFFFFF",
                        padding: "6px 8px",
                        fontSize: "14px",
                      }}
                      onClick={() => window.location.replace(`/${boards.id}`)}
                    >
                      <div title={boards.name} className="w-full">
                        {boards.name.length > 28
                          ? `${boards.name.substring(0, 28)}...`
                          : boards.name}
                      </div>
                    </div>
                    {/* </Link> */}
                  </div>
                </>
              ))}
          </div>
          <Divider
            orientation="vertical"
            flexItem
            style={{ backgroundColor: "rgba(255, 255, 255, 0.40)" }}
          />
          <div className="overflow-auto flex">
            <div className="h-fit flex mt-[10px]">
              {this.state.fetchingDataLoader && <Loader />}
              {!!this.state.allList.length &&
                this.state.allList.map((list) => (
                  <div
                    key={list.id}
                    className="w-[300px] h-[100%] ml-6 shadow-md bg-slate-100 rounded-[12px] p-4"
                  >
                    <p key="p" className="text-lg font-semibold ">
                      {list.name}
                      <DeleteForeverIcon
                        className="hover:text-red-600"
                        style={{
                          cursor: "pointer",
                          marginRight: "4px",
                          float: "right",
                        }}
                        onClick={(id) => {
                          this.deleteList(list.id);
                        }}
                      ></DeleteForeverIcon>
                    </p>
                    <hr />
                    <div className="snap-x mt-2">
                      <Cards listid={list.id}></Cards>
                    </div>
                  </div>
                ))}
              <Button
                variant="text"
                onClick={this.handleClickOpen}
                className="bg-slate-200"
                sx={{
                  height: "48px",
                  width: "272px",
                  marginLeft: "24px",
                  marginTop: "10px",
                  marginRight: "24px",
                  background: "#FFFFFF3D",
                  textTransform: "none",
                  borderRadius: "12px",
                  color: "#FFFFFF",
                  textAlign: "initial",
                  padding: "6px 8px",
                  fontSize: "14px",
                }}
              >
                + Add another list
              </Button>
            </div>
          </div>
          <Stack>
            <Dialog open={this.state.open} onClose={this.handleClose}>
              <DialogTitle>Add a List</DialogTitle>
              <DialogContent>
                <TextField
                  autoFocus
                  margin="dense"
                  id="name"
                  label="Enter list title…"
                  type="text"
                  fullWidth
                  variant="outlined"
                  onChange={this.newListName}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose}>Cancel</Button>
                <Button onClick={this.createButton}>Create</Button>
              </DialogActions>
            </Dialog>
          </Stack>
        </div>
        {/* </div> */}
      </>
    );
  }
}

export default NewBoard;
