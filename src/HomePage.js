import React, { Component } from "react";
import HeroSection from "./Component/HeroSection";
import Navbar from "./Component/Navbar";
import YourWorkSpace from "./Component/YourWorkSpace";

class HomePage extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <HeroSection />
      </div>
    );
  }
}

export default HomePage;
