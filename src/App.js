import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
// import { Route } from "router";
import "./App.css";
import NewBoard from "./Component/NewBoard";
import HomePage from "./HomePage";

function App() {
  return (
    <>
      <BrowserRouter>
        <Route path="/" exact>
          <HomePage />
        </Route>
        <Route path="/:id" exact component={NewBoard}></Route>
      </BrowserRouter>
    </>
  );
}

export default App;
